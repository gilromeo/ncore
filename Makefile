# Project Variables
PROJECT_NAME ?= ncore
ORG_NAME ?= ncore
REPO_NAME ?= ncore

.PHONY migrations db

migrations:
	cd ./ncore.Data && dotnet ef --startup-project ../ncore.Web/ migrations add $(mname) && cd ..

db: 
	cd ./ncore.Data && dotnet ef --startup-project ../ncore.Web/ database update && cd ..
